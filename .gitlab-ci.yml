###############################################################################
# GitLab CI YAML Documentation: https://docs.gitlab.com/ee/ci/yaml/
#
# Using badge colors from https://shields.io
# - Green:  #44cc11
# - Yellow: #dfb317
# - Red:    #e05d44
###############################################################################

variables:
  PIP_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pip"
  POETRY_VERSION: "1.3.2"
  POETRY_HOME: "$CI_PROJECT_DIR/.poetry"
  POETRY_CACHE_DIR: "$CI_PROJECT_DIR/.cache/pypoetry"

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: '$CI_COMMIT_BRANCH || $CI_COMMIT_TAG'

default:
  tags:
    - docker
  image: python:3.7
  interruptible: true
  cache:
    key: "$CI_JOB_NAME-$CI_COMMIT_REF_SLUG"
    paths:
      - ${POETRY_HOME}/
      - .cache/
  before_script:
    # Install Poetry
    - /usr/local/bin/python -c "import urllib.request; urllib.request.urlretrieve('https://install.python-poetry.org', 'install-poetry.py')"
    - /usr/local/bin/python ./install-poetry.py --version ${POETRY_VERSION}
    - export PATH="${POETRY_HOME}/bin:${PATH}"
    - which poetry
    - |
      if poetry --version ; then
          echo "Install seems okay";
      else
          /usr/local/bin/python ./install-poetry.py --uninstall;
          /usr/local/bin/python ./install-poetry.py --version ${POETRY_VERSION};
          poetry --version;
      fi
    # Install Poetry plugins
    - poetry self add "poetry-dynamic-versioning[plugin]==0.21.3"

stages:
  - lint
  - build
  - test
  - deploy

###############################################################################
# STYLE & LINT
###############################################################################

pylint:
  stage: lint
  needs: []
  dependencies: []
  script:
    # Setup dev env
    - poetry install -v --sync --only main,ci,dev,test
    - source $(poetry env info --path)/bin/activate
    - pip install 'pylint-gitlab>=1.1.0'

    - anybadge -l pylint -v 'fail' -c '#e05d44' -o -f badge.svg
    - python_files=$(git ls-files '*.py' | tr '\n' ' ')
    # Gitlab CodeQuality report
    - pylint --rcfile=.pylintrc --exit-zero --output-format=pylint_gitlab.GitlabCodeClimateReporter ${python_files} > codeclimate.json
    # Text file output
    - pylint --rcfile=.pylintrc ${python_files} | tee ./pylint_out.txt || pylint-exit $?
    - PYLINT_SCORE=$(cat ./pylint_out.txt | grep -oP 'Your code has been rated at \K(\d+\.*\d+)')
    - anybadge -v $PYLINT_SCORE -o -f badge.svg pylint
    - echo "pylint_rating $PYLINT_SCORE" > metrics.txt
  artifacts:
    when: always
    paths:
      - badge.svg
      - pylint_out.txt
      - codeclimate.json
      - metrics.txt
    reports:
      metrics: metrics.txt
      codequality: codeclimate.json

format_black:
  stage: lint
  needs: []
  dependencies: []
  script:
    - poetry install -v --sync --only ci,dev
    - source $(poetry env info --path)/bin/activate

    - anybadge -l format -v 'fail' -c '#e05d44' -o -f badge.svg
    - black --check ./
    - anybadge -l format -v ' ok ' -c '#44cc11' -o -f badge.svg
  allow_failure: true
  artifacts:
    when: always
    paths:
      - badge.svg

###############################################################################
# BUILD
###############################################################################

build:
  stage: build
  needs: []
  dependencies: []
  script:
    - poetry install -v --sync --only main
    - poetry build
    - poetry export --with dev,test > dev-requirements.txt
  artifacts:
    paths:
      - dist/*
      - dev-requirements.txt

###############################################################################
# TEST
###############################################################################

pytest:
  stage: test
  image: $IMAGE
  needs: []
  dependencies: []
  script:
    - poetry install -v --sync --only ci,multienv
    - poetry run tox -e ${TOXENV} | tee pytest_session.out

    - coverage=$(grep -oP 'TOTAL.*\s+\K(\d+\.\d+)%' ./pytest_session.out)
    - echo "coverage{env=\"${TOXENV}\"} ${coverage}" > metrics.txt
  artifacts:
    when: always
    paths:
      - ./.coverage.*
      - pytest_out*
      - coverage.xml
      - metrics.txt
    reports:
      metrics: metrics.txt
      junit: ./*.xml.junit
  parallel:
    matrix:
      - IMAGE: python:3.7
        TOXENV: py37
      - IMAGE: python:3.8
        TOXENV: py38
      - IMAGE: python:3.9
        TOXENV: py39
      - IMAGE: python:3.10
        TOXENV: py310
      - IMAGE: python:3.11
        TOXENV: py311

# We're using Poetry 1.2.x because it is starting to add support for plugins,
# like using the version number of a git tag in the pyproject.tml file.
# However Python 3.6 support was removed after Poetry 1.1.13.
# So this job does things a 'manually', using virtual environments, pip, and a
# requirements.txt generated from an earlier job, to accomplish the same thing
# as the above pytest jobs.
pytest-36:
  stage: test
  image: python:3.6.8
  needs:
    - job: build
      artifacts: true
  variables:
    COVERAGE_FILE: .coverage.py36
  before_script:
    - python3 -m venv ./.venv
    - source ./.venv/bin/activate
    - python -m pip install --upgrade pip
    - pip install -r ./dev-requirements.txt  --require-hashes
    - pip install dist/valgrind_codequality-*.whl
  script:
    - pytest
      --cov=valgrind_codequality
      --cov=tests
      --cov-report=term-missing
      --cov-branch
      -c pytest.ini
      --log-file=./pytest_out_py36.log
      --junit-xml=./pytest_out_py36.xml.junit
  artifacts:
    when: always
    paths:
      - ./.coverage.*
      - pytest_out*
    reports:
      junit: ./*.xml.junit

report:
  stage: test
  only:
    - merge_requests
  needs: []
  dependencies: []
  script:
    # Build test code
    - apt-get update && apt-get install -y cmake build-essential valgrind
    - mkdir -p tests/code/build
    - cd tests/code/build
    - cmake .. -DCMAKE_BUILD_TYPE=Debug
    - make
    # Run valgrind
    - cd $CI_PROJECT_DIR
    - valgrind --tool=memcheck --show-leak-kinds=all --track-origins=yes --verbose --xml=yes --xml-file=valgrind_out.xml tests/code/build/leak_code
    # Run codequality
    - poetry install -v --sync --only main
    - poetry run valgrind-codequality --input-file valgrind_out.xml --output-file valgrind.json
  artifacts:
    reports:
      codequality: valgrind.json
    paths:
      - valgrind_out.xml
      - valgrind.json

coverage:
  stage: test
  needs:
    - job: pytest
      artifacts: true
    - job: pytest-36
      artifacts: true
  script:
    - poetry install -v --sync --only ci,multienv
    - poetry run tox -e report
  coverage: /(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/
  artifacts:
    paths:
      - coverage.xml
      - htmlcov/
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage.xml
###############################################################################
# DEPLOY
###############################################################################

pypi_publish:
  stage: deploy
  rules:
    - if: $CI_COMMIT_TAG
  needs:
    - job: pylint
      artifacts: false
    - job: build
      artifacts: false
    - job: pytest
      artifacts: false
  script:
    - poetry install -v --sync
    - poetry build
    - poetry publish -u ArnaudM -p "${PYPI_MDP}"
  artifacts:
    paths:
      - dist/*
