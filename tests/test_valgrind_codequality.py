# -*- coding: utf-8 -*-
import json
import logging
import os
import sys

import pytest

import valgrind_codequality as uut

# PYTEST PLUGINS
# - pytest-console-scripts
# - pytest-cov


pytest_plugins = "console-scripts"

valgrind_XML_ERRORS_START = r"""<?xml version="1.0"?>

<valgrindoutput>

<protocolversion>4</protocolversion>
<protocoltool>memcheck</protocoltool>

<preamble>
  <line>Memcheck, a memory error detector</line>
  <line>Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.</line>
  <line>Using Valgrind-3.15.0-608cb11914-20190413 and LibVEX; rerun with -h for copyright info</line>
  <line>Command: ./tests/code/build/leak_code</line>
</preamble>

<pid>782</pid>
<ppid>445</ppid>
<tool>memcheck</tool>

<args>
  <vargv>
    <exe>/usr/bin/valgrind.bin</exe>
    <arg>--tool=memcheck</arg>
    <arg>--show-leak-kinds=all</arg>
    <arg>--track-origins=yes</arg>
    <arg>--verbose</arg>
    <arg>--xml=yes</arg>
    <arg>--xml-file=tests/valgrind-memcheck.xml</arg>
  </vargv>
  <argv>
    <exe>./tests/code/build/leak_code</exe>
  </argv>
</args>

<status>
  <state>RUNNING</state>
  <time>00:00:00:00.033 </time>
</status>"""

valgrind_XML_ERRORS_END = r"""</valgrindoutput>"""

log = logging.getLogger(__name__)


@pytest.mark.script_launch_mode("subprocess")
def test_cli_opts(script_runner):

    import_loc = uut.__file__
    log.info("Imported %s", import_loc)

    script_runner.run(
        "sed",
        "-i",
        f"s%/root/valgrind-codequality%{os.getcwd()}%g",
        "./tests/valgrind_simple.xml",
    )
    script_runner.run(
        "sed",
        "-i",
        f"s%/root/valgrind-codequality%{os.getcwd()}%g",
        "./tests/valgrind_concat.json",
    )

    ret = script_runner.run(
        os.path.abspath(sys.executable), "-m", "valgrind_codequality", "-h"
    )
    assert ret.success

    ret = script_runner.run("valgrind-codequality", "-h")
    assert ret.success

    ret = script_runner.run("valgrind-codequality", "-i")
    assert not ret.success

    ret = script_runner.run(
        "valgrind-codequality", "--input-file", "./tests/no-file.xml"
    )
    assert not ret.success

    ret = script_runner.run(
        "valgrind-codequality",
        "--input-file",
        "./tests/valgrind_simple.xml",
        "-c",
        "concat.json",
    )
    assert not ret.success

    ret = script_runner.run(
        "valgrind-codequality", "--input-file", "./tests/valgrind_simple.xml"
    )
    assert ret.success

    ret = script_runner.run(
        "valgrind-codequality", "-i", "./tests/valgrind_simple.xml", "-o"
    )
    assert not ret.success

    ret = script_runner.run(
        "valgrind-codequality",
        "-i",
        "./tests/valgrind_simple.xml",
        "-o",
        "valgrind.json",
    )
    assert ret.success

    ret = script_runner.run(
        "valgrind-codequality",
        "-i",
        "./tests/valgrind_simple.xml",
        "-c",
        "./tests/valgrind_concat.json",
        "-o",
        "valgrind.json",
    )
    assert ret.success

    ret = script_runner.run("valgrind-codequality", "--version")
    assert ret.success


def test_run_as_module():
    import subprocess

    ret = subprocess.run(
        ["python", "-m", "valgrind_codequality", "--version"], shell=True, check=True
    )
    assert ret.returncode == 0


def test_convert_no_messages(caplog):
    xml_in = valgrind_XML_ERRORS_START + valgrind_XML_ERRORS_END
    assert uut._convert(xml_in, base_dirs=[os.getcwd()]) == ("[]", 0)

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" in caplog.text
    assert "Nothing to do" in caplog.text

    assert "ERROR" not in caplog.text


def test_convert_severity_minor(caplog):
    xml_in = valgrind_XML_ERRORS_START
    xml_in += r"""<error>
  <unique>0x3</unique>
  <tid>1</tid>
  <kind>UninitCondition</kind>
  <what>Conditional jump or move depends on uninitialised value(s)</what>
  <stack>
    <frame>
      <ip>0x48CB228</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>__vfprintf_internal</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>vfprintf-internal.c</file>
      <line>1687</line>
    </frame>
    <frame>
      <ip>0x48B4D3E</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>printf</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>printf.c</file>
      <line>33</line>
    </frame>
    <frame>
      <ip>0x1091EA</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>8</line>
    </frame>
  </stack>
  <auxwhat>Uninitialised value was created by a stack allocation</auxwhat>
  <stack>
    <frame>
      <ip>0x1091C9</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>6</line>
    </frame>
  </stack>
</error>"""
    xml_in = xml_in.replace("/root/valgrind-codequality", os.getcwd())
    xml_in += valgrind_XML_ERRORS_END

    json_str, num_cq_issues = uut._convert(xml_in, base_dirs=[os.getcwd()])
    json_out = json.loads(json_str)
    # print(json_out)

    assert len(json_out) == num_cq_issues
    out = json_out[0]
    assert "UninitCondition" in out["description"]
    assert out["severity"] == "minor"
    assert out["location"]["path"] == os.getcwd() + "/tests/code/leak_code.c"
    assert out["location"]["lines"]["begin"] == 8

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" not in caplog.text
    assert "ERROR" not in caplog.text


def test_convert_empty(caplog):
    xml_in = valgrind_XML_ERRORS_START
    xml_in += r"""<error>
  <unique>0x3</unique>
  <tid>1</tid>
  <kind>UninitCondition</kind>
  <what>Conditional jump or move depends on uninitialised value(s)</what>
  <stack>
  </stack>
</error>"""
    xml_in = xml_in.replace("/root/valgrind-codequality", os.getcwd())
    xml_in += valgrind_XML_ERRORS_END

    json_str, num_cq_issues = uut._convert(xml_in, base_dirs=[os.getcwd()])
    json_out = json.loads(json_str)
    # print(json_out)

    assert len(json_out) == num_cq_issues
    assert 0 == num_cq_issues

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" in caplog.text
    assert "ERROR" not in caplog.text


def test_convert_severity_blocker(caplog):
    xml_in = valgrind_XML_ERRORS_START
    xml_in += r"""<error>
  <unique>0x8</unique>
  <tid>1</tid>
  <kind>Leak_StillReachable</kind>
  <xwhat>
    <text>4 bytes in 1 blocks are still reachable in loss record 1 of 1</text>
    <leakedbytes>4</leakedbytes>
    <leakedblocks>1</leakedblocks>
  </xwhat>
  <stack>
    <frame>
      <ip>0x483B7F3</ip>
      <obj>/usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so</obj>
      <fn>malloc</fn>
    </frame>
    <frame>
      <ip>0x109202</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>11</line>
    </frame>
  </stack>
</error>"""
    xml_in = xml_in.replace("/root/valgrind-codequality", os.getcwd())
    xml_in += valgrind_XML_ERRORS_END

    json_str, num_cq_issues = uut._convert(xml_in, base_dirs=[os.getcwd()])
    json_out = json.loads(json_str)
    assert len(json_out) == num_cq_issues
    out = json_out[0]
    assert out["severity"] == "minor"

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" not in caplog.text
    assert "ERROR" not in caplog.text


def test_convert_multiple_errors(caplog):
    xml_in = valgrind_XML_ERRORS_START
    xml_in += r"""<error>
  <unique>0x3</unique>
  <tid>1</tid>
  <kind>UninitCondition</kind>
  <what>Conditional jump or move depends on uninitialised value(s)</what>
  <stack>
    <frame>
      <ip>0x48CB228</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>__vfprintf_internal</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>vfprintf-internal.c</file>
      <line>1687</line>
    </frame>
    <frame>
      <ip>0x48B4D3E</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>printf</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>printf.c</file>
      <line>33</line>
    </frame>
    <frame>
      <ip>0x1091EA</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>8</line>
    </frame>
  </stack>
  <auxwhat>Uninitialised value was created by a stack allocation</auxwhat>
  <stack>
    <frame>
      <ip>0x1091C9</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>6</line>
    </frame>
  </stack>
</error>"""
    xml_in += r"""<error>
  <unique>0x8</unique>
  <tid>1</tid>
  <kind>Leak_DefinitelyLost</kind>
  <xwhat>
    <text>4 bytes in 1 blocks are loss record 1 of 1</text>
    <leakedbytes>4</leakedbytes>
    <leakedblocks>1</leakedblocks>
  </xwhat>
  <stack>
    <frame>
      <ip>0x483B7F3</ip>
      <obj>/usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so</obj>
      <fn>malloc</fn>
    </frame>
    <frame>
      <ip>0x109202</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>11</line>
    </frame>
  </stack>
</error>"""
    xml_in = xml_in.replace("/root/valgrind-codequality", os.getcwd())
    xml_in += valgrind_XML_ERRORS_END

    json_str, num_cq_issues = uut._convert(xml_in, base_dirs=[os.getcwd()])
    json_out = json.loads(json_str)

    assert len(json_out) == num_cq_issues
    assert json_out[0]["severity"] == "minor"
    assert (
        json_out[0]["description"]
        == "UninitCondition: Conditional jump or move depends on uninitialised value(s)"
    )
    assert json_out[1]["severity"] == "blocker"
    assert (
        json_out[1]["description"]
        == "Leak_DefinitelyLost: 4 bytes in 1 blocks are loss record 1 of 1"
    )

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" not in caplog.text
    assert "ERROR" not in caplog.text


def test_convert_location_file0(caplog):
    xml_in = valgrind_XML_ERRORS_START
    xml_in += r"""<error>
  <unique>0x3</unique>
  <tid>1</tid>
  <kind>UninitCondition</kind>
  <what>Conditional jump or move depends on uninitialised value(s)</what>
  <stack>
    <frame>
      <ip>0x48CB228</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>__vfprintf_internal</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>vfprintf-internal.c</file>
      <line>1687</line>
    </frame>
    <frame>
      <ip>0x48B4D3E</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>printf</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>printf.c</file>
      <line>33</line>
    </frame>
    <frame>
      <ip>0x1091EA</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>8</line>
    </frame>
  </stack>
  <auxwhat>Uninitialised value was created by a stack allocation</auxwhat>
  <stack>
    <frame>
      <ip>0x1091C9</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>6</line>
    </frame>
  </stack>
</error>"""
    xml_in = xml_in.replace("/root/valgrind-codequality", os.getcwd())
    xml_in += valgrind_XML_ERRORS_END
    print(xml_in)

    json_str, num_cq_issues = uut._convert(xml_in, base_dirs=[os.getcwd()])
    json_out = json.loads(json_str)

    assert len(json_out) == num_cq_issues
    assert json_out[0]["location"]["path"] == os.getcwd() + "/tests/code/leak_code.c"

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" not in caplog.text
    assert "ERROR" not in caplog.text


def test_source_line_extractor_warning(caplog):
    xml_in = valgrind_XML_ERRORS_START
    xml_in += r"""<error>
  <unique>0x3</unique>
  <tid>1</tid>
  <kind>UninitCondition</kind>
  <what>Conditional jump or move depends on uninitialised value(s)</what>
  <stack>
    <frame>
      <ip>0x48CB228</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>__vfprintf_internal</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>vfprintf-internal.c</file>
      <line>1687</line>
    </frame>
    <frame>
      <ip>0x48B4D3E</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>printf</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>printf.c</file>
      <line>33</line>
    </frame>
    <frame>
      <ip>0x1091EA</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>24</line>
    </frame>
  </stack>
  <auxwhat>Uninitialised value was created by a stack allocation</auxwhat>
  <stack>
    <frame>
      <ip>0x1091C9</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>6</line>
    </frame>
  </stack>
</error>"""
    xml_in = xml_in.replace("/root/valgrind-codequality", os.getcwd())
    xml_in += valgrind_XML_ERRORS_END

    with caplog.at_level(logging.WARNING):
        json.loads(uut._convert(xml_in, base_dirs=[os.getcwd()])[0])

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" in caplog.text
    assert "ERROR" not in caplog.text


def test_convert_exclude(caplog):
    xml_in = valgrind_XML_ERRORS_START
    xml_in += r"""<error>
  <unique>0x3</unique>
  <tid>1</tid>
  <kind>UninitCondition</kind>
  <what>Conditional jump or move depends on uninitialised value(s)</what>
  <stack>
    <frame>
      <ip>0x48CB228</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>__vfprintf_internal</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>vfprintf-internal.c</file>
      <line>1687</line>
    </frame>
    <frame>
      <ip>0x48B4D3E</ip>
      <obj>/usr/lib/x86_64-linux-gnu/libc-2.31.so</obj>
      <fn>printf</fn>
      <dir>/build/glibc-SzIz7B/glibc-2.31/stdio-common</dir>
      <file>printf.c</file>
      <line>33</line>
    </frame>
    <frame>
      <ip>0x1091EA</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>8</line>
    </frame>
  </stack>
  <auxwhat>Uninitialised value was created by a stack allocation</auxwhat>
  <stack>
    <frame>
      <ip>0x1091C9</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>6</line>
    </frame>
  </stack>
</error>"""
    xml_in += r"""<error>
  <unique>0x8</unique>
  <tid>1</tid>
  <kind>Leak_StillReachable</kind>
  <xwhat>
    <text>4 bytes in 1 blocks are still reachable in loss record 1 of 1</text>
    <leakedbytes>4</leakedbytes>
    <leakedblocks>1</leakedblocks>
  </xwhat>
  <stack>
    <frame>
      <ip>0x483B7F3</ip>
      <obj>/usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so</obj>
      <fn>malloc</fn>
    </frame>
    <frame>
      <ip>0x109208</ip>
      <obj>/root/valgrind-codequality/exclude/exclude.so</obj>
    </frame>
    <frame>
      <ip>0x109202</ip>
      <obj>/root/valgrind-codequality/tests/code/build/leak_code</obj>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/tests/code</dir>
      <file>leak_code.c</file>
      <line>11</line>
    </frame>
  </stack>
</error>"""
    xml_in += r"""<error>
  <unique>0x8</unique>
  <tid>1</tid>
  <kind>Leak_StillReachable</kind>
  <xwhat>
    <text>4 bytes in 1 blocks are still reachable in loss record 1 of 1</text>
    <leakedbytes>4</leakedbytes>
    <leakedblocks>1</leakedblocks>
  </xwhat>
  <stack>
    <frame>
      <ip>0x483B7F3</ip>
      <obj>/usr/lib/x86_64-linux-gnu/valgrind/vgpreload_memcheck-amd64-linux.so</obj>
      <fn>malloc</fn>
    </frame>
    <frame>
      <ip>0x109202</ip>
      <fn>main</fn>
      <dir>/root/valgrind-codequality/exclude/code</dir>
      <file>leak_code.c</file>
      <line>11</line>
    </frame>
  </stack>
</error>"""
    xml_in = xml_in.replace("/root/valgrind-codequality", os.getcwd())
    xml_in += valgrind_XML_ERRORS_END

    json_str, num_cq_issues = uut._convert(
        xml_in, base_dirs=[os.getcwd()], exclude_list=["exclude"]
    )
    json_out = json.loads(json_str)

    assert len(json_out) == num_cq_issues
    assert 1 == num_cq_issues
    assert json_out[0]["severity"] == "minor"
    assert (
        json_out[0]["description"]
        == "UninitCondition: Conditional jump or move depends on uninitialised value(s)"
    )

    print("Captured log:\n", caplog.text, flush=True)
    assert "WARNING" not in caplog.text
    assert "ERROR" not in caplog.text
