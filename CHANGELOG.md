# `VALGRIND-CODEQUALITY` CHANGELOG <!-- omit in toc -->

Table of Contents:
- [v1.2.1 - 2023-05-30](#v121---2023-05-30)
- [v1.2.0 - 2023-05-29](#v120---2023-05-29)
- [v1.1.0 - 2023-02-02](#v110---2023-02-02)
- [v1.0.3 - 2023-02-19](#v103---2023-02-19)
- [v1.0.2 - 2023-02-03](#v102---2023-02-03)
- [v1.0.1 - 2023-01-31](#v101---2023-01-31)
- [v1.0.0 - 2023-01-28](#v100---2023-01-28)

## v1.2.1 - 2023-05-30

* Fix crash when 'obj' doesn't exist in 'frame' with exclude option

## v1.2.0 - 2023-05-29

* Add option to exclude leak from path name

## v1.1.0 - 2023-02-02

* Fix changelog date
* Change base directory mechanism

## v1.0.3 - 2023-02-19

* Change leak severity
* Fix path not in project folders
* Add valgrind option in doc

## v1.0.2 - 2023-02-03

* Fix docs
* Rewrite changelog file 

## v1.0.1 - 2023-01-31

* Fix default value of option relative-dir

## v1.0.0 - 2023-01-28

* First version.
